<?php


namespace XiHe\Tests\Publisher;

use PHPUnit\Framework\TestCase;
use XiHe\Message\Quality;
use XiHe\Message\Value;
use XiHe\Publisher\Publisher;
use XiHe\Transport\BaseTransport;
use XiHe\Transport\TransportInterface;


class PublisherTest extends TestCase
{
    /** @var  Publisher */
    protected $publisher;

    public function testBasicPublishing()
    {
        $this->havingPublisher();
        $this->havingTransports(5);
        $this->whenWePublishAMessage();
        $this->thenNoErrorOccurs();
    }

    public function testPublishingOnChannels()
    {
        $this->havingPublisher();
        $this->havingTransports(5);
        $this->whenWePublishAMessageOnChannels(array('testchannel1', 'testchannel2'));
        $this->thenNoErrorOccurs();
    }


    public function testGetTransportEmpty()
    {
        $this->havingPublisher();
        $this->thenPublisherHasTransports(array());
    }

    public function testAddTransport()
    {
        $this->havingPublisher();
        $this->whenWeAddATransport($firstTransport = new BaseTransport());
        $this->whenWeAddATransport($secondTransport = new BaseTransport());
        $this->whenWeAddATransport($firstTransport);
        $this->thenPublisherHasTransports(array($firstTransport, $secondTransport));
    }

    public function testRemoveTransport()
    {
        $this->havingPublisher();
        $this->whenWeAddATransport($firstTransport = new BaseTransport());
        $this->whenWeAddATransport($secondTransport = new BaseTransport());
        $this->whenWeRemoveATransport($firstTransport);
        $this->thenPublisherHasTransports(array($secondTransport));
    }

    protected function havingPublisher()
    {
        $this->publisher = new Publisher();
    }

    protected function havingTransports($numberOfTransports)
    {
        for ($i = 0; $i < $numberOfTransports; $i++) {
            $transport = $this->getMockBuilder(BaseTransport::class)
                ->setMethods(['send'])
                ->getMock();
            $transport->expects($this->once())
                ->method('send')
                ->willReturnCallback(function ($channels, $event, $payload) {
                    $this->assertEquals(array('testchannel1', 'testchannel2'), $channels);
                    $this->assertEquals('XiHe.Message.Value', $event);
                    $this->assertArraySubset(Array(
                        'timeStamp' => '2017-11-13 22:05:13.999613',
                        'value' => 123,
                        'quality' => Quality::GoodMoreData
                    ), $payload);
                    return true;
                });
            $this->publisher->addTransport($transport);
        }
    }

    protected function whenWePublishAMessage()
    {
        $timeStamp = \DateTime::createFromFormat('Y-m-d H:i:s.u', '2017-11-13 22:05:13.999613');
        $event = new Value(123, Quality::GoodMoreData, $timeStamp);
        $event->setPublishOnChannels(array('testchannel1', 'testchannel2'));
        $this->publisher->publish($event);
    }

    protected function whenWePublishAMessageOnChannels($channels)
    {
        $timeStamp = \DateTime::createFromFormat('Y-m-d H:i:s.u', '2017-11-13 22:05:13.999613');
        $event = new Value(123, Quality::GoodMoreData, $timeStamp);
        $this->publisher->publish($event, $channels);
    }

    protected function whenWeAddATransport(TransportInterface $transport)
    {
        $this->publisher->addTransport($transport);
    }

    protected function whenWeRemoveATransport(TransportInterface $transport)
    {
        $this->publisher->removeTransport($transport);
    }

    protected function thenNoErrorOccurs()
    {
        $this->assertTrue(true);
    }

    protected function thenPublisherHasTransports(array $transports)
    {
        $this->assertEquals($transports, $this->publisher->getTransports());
    }
}