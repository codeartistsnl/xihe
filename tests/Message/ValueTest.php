<?php

namespace XiHe\Tests\Message;

use PHPUnit\Framework\TestCase;
use XiHe\Message\Quality;
use XiHe\Message\Value;

class ValueTest extends TestCase
{
    public function testConstructDefault()
    {
        $value = new Value(123);
        $this->assertEquals(123, $value->value);
        $this->assertEquals(Quality::Good, $value->quality);
        $this->assertEquals(date('Y-m-d h:i:s'), $value->timeStamp->format('Y-m-d h:i:s'));
    }

    public function testConstructQuality()
    {
        $value = new Value(123, Quality::UncertainDataSubNormal);
        $this->assertEquals(123, $value->value);
        $this->assertEquals(Quality::UncertainDataSubNormal, $value->quality);
        $this->assertEquals(date('Y-m-d h:i:s'), $value->timeStamp->format('Y-m-d h:i:s'));
    }

    public function testConstructTimeStamp()
    {
        $ts = \DateTime::createFromFormat('Y-m-d H:i:s', '2017-11-14 23:55:56');
        $value = new Value(123, Quality::UncertainDataSubNormal, $ts);
        $this->assertEquals(123, $value->value);
        $this->assertEquals(Quality::UncertainDataSubNormal, $value->quality);
        $this->assertEquals($ts, $value->timeStamp);
    }

    public function testTransformToArray()
    {
        $timeStamp = \DateTime::createFromFormat('Y-m-d H:i:s.u', '2017-11-14 23:55:56.123456');
        $value = new Value(123, Quality::UncertainDataSubNormal, $timeStamp);
        $this->assertEquals(
            array(
                'value' => 123,
                'timeStamp' => '2017-11-14 23:55:56.123456',
                'quality' => Quality::UncertainDataSubNormal,
                'messageId' => $value->getMessageId(),
                'class' => 'XiHe\Message\Value'
            ), $value->transformToArray()
        );
    }

    public function testPublishAsDefault()
    {
        $value = new Value(123);
        $this->assertEquals('XiHe.Message.Value', $value->getPublishAs());
    }

    public function testSetPublishAs()
    {
        $value = new Value(123);
        $value->setPublishAs('test');
        $this->assertEquals('test', $value->getPublishAs());
    }


    public function testPublishOnChannelsDefault()
    {
        $value = new Value(123);
        $this->assertEquals(array(), $value->getPublishOnChannels());
    }

    public function testSetPublishOnChannels()
    {
        $value = new Value(123);
        $value->setPublishOnChannels(array('c1', 'c2'));
        $this->assertEquals(array('c1', 'c2'), $value->getPublishOnChannels());
    }
}