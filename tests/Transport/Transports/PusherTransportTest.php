<?php


namespace XiHe\Tests\PublisherSender\Senders;

use PHPUnit\Framework\TestCase;
use XiHe\Transport\Transports\PusherTransport;


class PusherTransportTest extends TestCase
{
    /** @var  PusherTransport */
    protected $sender;

    public function testSend()
    {
        $this->havingPusherTransport();
        $this->whenWePublishAnEventWithPayLoad(array('eventData'));
        $this->thenNoErrorOccurs();
    }


    protected function whenWePublishAnEventWithPayLoad($payLoad)
    {
        $this->sender->send(array('testchannel1', 'testchannel2'), 'testEvent', $payLoad);
    }

    public function havingPusherTransport()
    {
        $pusher= $this->getMockBuilder(\Pusher::class)
            ->setMethods(array('trigger'))
            ->setConstructorArgs(array('key', 'secret', ' app_id', []))
            ->getMock();
        $pusher->expects($this->once())
            ->method('trigger')
            ->willReturnCallback(function ($channels, $event, $payload) {
                $this->assertEquals(array('testchannel1', 'testchannel2'), $channels);
                $this->assertEquals('testEvent', $event);
                $this->assertEquals(array('eventData'), $payload);
            });
        $this->sender = new PusherTransport();
        $this->sender->setPusher($pusher);
    }

    protected function thenNoErrorOccurs()
    {
        $this->assertTrue(true);
    }
}