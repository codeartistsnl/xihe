<?php

namespace XiHe\Publisher;


interface PublishableMessageInterface
{
    /**
     * Get the channels the message should be published on
     *
     * @return array
     */
    public function getPublishOnChannels(): array;

    /**
     * Sets the channels the message should be published on
     *
     * @param array $channels
     */
    public function setPublishOnChannels(array $channels);

}
