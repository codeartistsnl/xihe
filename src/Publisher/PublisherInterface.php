<?php

namespace XiHe\Publisher;

interface PublisherInterface
{
    /**
     * Publishes the message
     *
     * @param PublishableMessageInterface $message
     * @param array|null $channels
     * @return mixed
     */
    public function publish(PublishableMessageInterface $message, ?array $channels = null);
}