<?php

namespace XiHe\Publisher;


interface TransformerInterface
{
    /**
     * Get the data to publish as an array
     *
     * @return array
     */
    public function transformToArray(): array;
}
