<?php

namespace XiHe\Publisher;


use XiHe\Transport\BaseTransport;
use XiHe\Transport\HasTransportsTrait;


class Publisher implements PublisherInterface
{
    use HasTransportsTrait;

    public function publish(PublishableMessageInterface $message, ?array $channels = null)
    {
        if ($message instanceof PublishAsInterface) {
            $publishAs = $message->getPublishAs();
        } else {
            $publishAs = \get_class($message);
        }
        /** @var BaseTransport $sender */
        foreach ($this->getTransports() as $sender) {
            $sender->send($channels ?: $message->getPublishOnChannels(), $publishAs, $this->transformToArray($message));
        }
    }


    /**
     * Get the the given message as an array
     *
     * @param  PublishableMessageInterface $message
     * @return array
     */
    protected function transformToArray(PublishableMessageInterface $message): array
    {
        if ($message instanceof TransformerInterface) {
            return $message->transformToArray();
        }

        $payload = [];
        foreach ((new \ReflectionClass($message))->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $payload[$property->getName()] = $this->transformProperty($property->getValue($message));
        }

        return $payload;
    }

    /**
     * Format the given value for a property.
     *
     * @param  mixed $value
     * @return mixed
     */
    protected function transformProperty($value)
    {
        if ($value instanceof TransformerInterface) {
            return $value->transformToArray();
        }
        return $value;
    }
}