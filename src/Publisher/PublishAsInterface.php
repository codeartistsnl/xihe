<?php

namespace XiHe\Publisher;


interface PublishAsInterface
{
    /**
     * Gets the message publishing name.
     *
     * @return string
     */
    public function getPublishAs(): string;

    /**
     * Sets the message publishing name.
     *
     * @param string $publishAs
     */
    public function setPublishAs(string $publishAs);

}
