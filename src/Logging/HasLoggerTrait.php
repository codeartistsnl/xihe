<?php


namespace XiHe\Logging;

use Psr\Log\LoggerInterface;

trait HasLoggerTrait
{
    /** @var  LoggerInterface */
    protected $logger;

    public function getLogger(): LoggerInterface
    {
        if (!$this->logger) {
            $this->logger = new ConsoleLogger();
        }
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }

}