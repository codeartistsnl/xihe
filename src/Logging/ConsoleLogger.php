<?php


namespace XiHe\Logging;

/**
 * Output coloring https://www.if-not-true-then-false.com
 */
use Psr\Log\LoggerInterface;

class ConsoleLogger implements LoggerInterface
{
    protected $foregroundColors = array();
    protected $backgroundColors = array();
    protected $useColors;

    public function __construct(?bool $useColors = true)
    {
        $this->useColors = $useColors;
        if ($useColors) {
            // Set up shell colors
            $this->foregroundColors['black'] = '0;30';
            $this->foregroundColors['dark_gray'] = '1;30';
            $this->foregroundColors['blue'] = '0;34';
            $this->foregroundColors['light_blue'] = '1;34';
            $this->foregroundColors['green'] = '0;32';
            $this->foregroundColors['light_green'] = '1;32';
            $this->foregroundColors['cyan'] = '0;36';
            $this->foregroundColors['light_cyan'] = '1;36';
            $this->foregroundColors['red'] = '0;31';
            $this->foregroundColors['light_red'] = '1;31';
            $this->foregroundColors['purple'] = '0;35';
            $this->foregroundColors['light_purple'] = '1;35';
            $this->foregroundColors['brown'] = '0;33';
            $this->foregroundColors['yellow'] = '1;33';
            $this->foregroundColors['light_gray'] = '0;37';
            $this->foregroundColors['white'] = '1;37';
            $this->backgroundColors['black'] = '40';
            $this->backgroundColors['red'] = '41';
            $this->backgroundColors['green'] = '42';
            $this->backgroundColors['yellow'] = '43';
            $this->backgroundColors['blue'] = '44';
            $this->backgroundColors['magenta'] = '45';
            $this->backgroundColors['cyan'] = '46';
            $this->backgroundColors['light_gray'] = '47';
        }
    }


    public function emergency($message, array $context = array())
    {
        $this->log('emergency', $message, $context);
    }

    public function log($level, $message, array $context = array())
    {
        $message = \str_pad($level, \strlen('emergency')) . ' ' . $message;
        if ($this->useColors) {
            $foregroundColor = 'white';
            $backgroundColor = 'black';
            switch ($level) {
                case 'emergency':
                case 'alert':
                    $foregroundColor = 'white';
                    $backgroundColor = 'red';
                    break;
                case 'critical':
                case 'error':
                    $foregroundColor = 'white';
                    $backgroundColor = 'magenta';
                    break;
                case 'warning':
                case 'notice':
                    $foregroundColor = 'black';
                    $backgroundColor = 'yellow';
                    break;
                case 'info':
                case 'debug':
                    $foregroundColor = 'white';
                    $backgroundColor = 'green';
                    break;
            }
            $message = $this->getColoredString($message, $foregroundColor, $backgroundColor);
        }
        echo $message . PHP_EOL;
    }

    public function getColoredString(string $string, ?string $foregroundColor = null, ?string $backgroundColor = null): string
    {
        $coloredString = "";
        // Check if given foreground color found
        if (isset($this->foregroundColors[$foregroundColor])) {
            $coloredString .= "\033[" . $this->foregroundColors[$foregroundColor] . "m";
        }
        // Check if given background color found
        if (isset($this->backgroundColors[$backgroundColor])) {
            $coloredString .= "\033[" . $this->backgroundColors[$backgroundColor] . "m";
        }
        // Add string and end coloring
        $coloredString .= $string . "\033[0m";
        return $coloredString;
    }

    public function alert($message, array $context = array())
    {
        $this->log('alert', $message, $context);
    }

    public function critical($message, array $context = array())
    {
        $this->log('critical', $message, $context);
    }

    public function error($message, array $context = array())
    {
        $this->log('error', $message, $context);
    }

    public function warning($message, array $context = array())
    {
        $this->log('warning', $message, $context);
    }

    public function notice($message, array $context = array())
    {
        $this->log('notice', $message, $context);
    }

    public function info($message, array $context = array())
    {
        $this->log('info', $message, $context);
    }

    public function debug($message, array $context = array())
    {
        $this->log('debug', $message, $context);
    }


}