<?php


namespace XiHe\Transport;


trait HasTransportsTrait
{
    /** @var  TransportInterface[] */
    protected $transports;

    public function addTransport(TransportInterface $transport)
    {
        if (!$this->hasTransport($transport)) {
            $this->transports[\spl_object_hash($transport)] = $transport;
        }
        return $this;
    }

    public function hasTransport(TransportInterface $transport): bool
    {
        if (!$this->transports) {
            return false;
        }
        return \array_key_exists(\spl_object_hash($transport), $this->transports);
    }

    public function removeTransport(TransportInterface $transport)
    {
        if ($this->hasTransport($transport)) {
            unset($this->transports[\spl_object_hash($transport)]);
        }
    }

    /**
     * @return TransportInterface[]
     */
    public function getTransports(): array
    {
        if (null === $this->transports) {
            $this->transports = array();
        }
        return \array_values($this->transports);
    }
}