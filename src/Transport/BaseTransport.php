<?php


namespace XiHe\Transport;


use XiHe\Exception\NotImplementedException;

class BaseTransport implements TransportInterface
{
    public function send(array $channels, string $messageName, array $payload = []): bool
    {
        throw new NotImplementedException();
    }

}