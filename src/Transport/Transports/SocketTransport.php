<?php

namespace XiHe\Transport\Transports;

use XiHe\Logging\HasLoggerTrait;
use XiHe\Transport\TransportInterface;


class SocketTransport implements TransportInterface
{
    use HasLoggerTrait;

    /** @var  resource */
    protected $socket;

    /**
     * @var string
     */
    protected $host;

    /**
     * @var int
     */
    protected $port;

    /** @var  bool */
    protected $connected;

    public function __construct(string $host, int $port)
    {
        $this->host = $host;
        $this->port = $port;
        $this->connected = false;
    }

    public function send(array $channels, string $messageName, array $payload = []): bool
    {
        $result = false;
        if (!$this->connected) {
            $this->connect();
        }
        if ($this->connected) {
            $result = @\socket_write($this->socket, \json_encode($payload));
            if ($result === false) {
                $errorCode = socket_last_error();
                $errorMsg = socket_strerror($errorCode);
                \socket_clear_error();
                $this->getLogger()->error("Socket write failed. Error $errorCode $errorMsg");
                \socket_close($this->socket);
                $this->connected = false;
                $socket = null;
            }
        }
        return $result !== false;
    }

    protected function connect()
    {
        $this->socket = \socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (false === @\socket_connect($this->socket, $this->host, $this->port)) {
            $errorCode = socket_last_error();
            $errorMsg = socket_strerror($errorCode);
            \socket_clear_error();
            $this->getLogger()->error("Connect failed to {$this->host}:{$this->port}. Error $errorCode $errorMsg");
            $this->connected = false;
        } else {

            $this->getLogger()->info("Connected to {$this->host}:{$this->port}");
            $this->connected = true;
        }
    }

}