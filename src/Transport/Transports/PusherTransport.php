<?php

namespace XiHe\Transport\Transports;

use Pusher;
use XiHe\Logging\HasLoggerTrait;
use XiHe\Transport\TransportInterface;

class PusherTransport implements TransportInterface
{
    use HasLoggerTrait;
    /**
     * @var Pusher
     */
    protected $pusher;

    /** @var  string */
    protected $key;

    /** @var  string */
    protected $secret;

    /** @var  string */
    protected $appId;

    /** @var  string */
    protected $options;

    public function __construct(?string $applicationKey = null, ?string $secret = null, ?string $appId = null, ?array $options = null)
    {
        $this->key = $applicationKey;
        $this->secret = $secret;
        $this->appId = $appId;
        $this->options = $options;
    }

    public function send(array $channels, string $messageName, array $payload = []): bool
    {
        return (bool)$this->getPusher()->trigger($channels, $messageName, $payload);
    }

    public function getPusher(): Pusher
    {
        if (!$this->pusher) {
            $this->pusher = new Pusher($this->key, $this->secret, $this->appId, $this->options);
        }
        return $this->pusher;
    }

    public function setPusher(Pusher $pusher)
    {
        $this->pusher = $pusher;
        return $this;
    }


}
