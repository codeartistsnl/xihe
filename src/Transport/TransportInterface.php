<?php


namespace XiHe\Transport;


interface TransportInterface
{
    /**
     * Sends the message
     *
     * @param array $channels An array of channel names to publish the message on.
     * @param string $messageName The name of the message. Clients may subscribe to messages with this name
     * @param array $payload The data of the message
     * @return bool true on success, false on error
     */
    public function send(array $channels, string $messageName, array $payload = []): bool;
}