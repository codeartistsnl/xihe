<?php

namespace XiHe\Message;

use XiHe\Publisher\TransformerInterface;

class Event extends AbstractMessage implements TransformerInterface
{
    /** @var \DateTimeInterface */
    public $timeStamp;

    /** @var string */
    public $message;

    /** @var int */
    public $severity;


    public function transformToArray(): array
    {
        return array_merge(
            parent::transformToArray(),
            array(
                'timeStamp' => $this->timeStamp->format('Y-m-d H:i:s.u'),
                'message' => $this->message,
                'severity' => $this->severity,
            )
        );
    }


}