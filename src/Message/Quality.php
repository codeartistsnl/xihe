<?php

namespace XiHe\Message;


class Quality
{
    const Good = '00000000';
    const GoodSubscriptionTransferred = '02D0000';
    const GoodCompletesAsynchronously = '02E0000';
    const GoodOverload = '02F0000';
    const GoodClamped = '0300000';
    const GoodLocalOverride = '0960000';
    const GoodEntryInserted = '0A20000';
    const GoodEntryReplaced = '0A30000';
    const GoodNoData = '0A50000';
    const GoodMoreData = '0A60000';
    const GoodCommunicationEvent = '0A70000';
    const GoodShutdownEvent = '0A80000';
    const GoodCallAgain = '0A90000';
    const GoodNonCriticalTimeout = '0AA0000';
    const GoodResultsMayBeIncomplete = '0BA0000';

    const Uncertain = '40000000';
    const UncertainReferenceOutOfServer = '406C0000';
    const UncertainNoCommunicationLastUsableValue = '408F0000';
    const UncertainLastUsableValue = '40900000';
    const UncertainSubstituteValue = '40910000';
    const UncertainInitialValue = '40920000';
    const UncertainSensorNotAccurate = '40930000';
    const UncertainEngineeringUnitsExceeded = '40940000';
    const UncertainSubNormal = '40950000';
    const UncertainDataSubNormal = '40A40000';
    const UncertainReferenceNotDeleted = '40BC0000';
    const UncertainNotAllNodesAvailable = '40C00000';

    const Bad = '80000000';
    const BadUnexpectedError = '80010000';
    const BadInternalError = '80020000';
    const BadOutOfMemory = '80030000';
    const BadResourceUnavailable = '80040000';
    const BadCommunicationError = '80050000';
    const BadEncodingError = '80060000';
    const BadDecodingError = '80070000';
    const BadEncodingLimitsExceeded = '80080000';
    const BadUnknownResponse = '80090000';
    const BadTimeout = '800A0000';
    const BadServiceUnsupported = '800B0000';
    const BadShutdown = '800C0000';
    const BadServerNotConnected = '800D0000';
    const BadServerHalted = '800E0000';
    const BadNothingToDo = '800F0000';
    const BadTooManyOperations = '80100000';
    const BadDataTypeIdUnknown = '80110000';
    const BadCertificateInvalid = '80120000';
    const BadSecurityChecksFailed = '80130000';
    const BadCertificateTimeInvalid = '80140000';
    const BadCertificateIssuerTimeInvalid = '80150000';
    const BadCertificateHostNameInvalid = '80160000';
    const BadCertificateUriInvalid = '80170000';
    const BadCertificateUseNotAllowed = '80180000';
    const BadCertificateIssuerUseNotAllowed = '80190000';
    const BadCertificateUntrusted = '801A0000';
    const BadCertificateRevocationUnknown = '801B0000';
    const BadCertificateIssuerRevocationUnknown = '801C0000';
    const BadCertificateRevoked = '801D0000';
    const BadCertificateIssuerRevoked = '801E0000';
    const BadUserAccessDenied = '801F0000';
    const BadIdentityTokenInvalid = '80200000';
    const BadIdentityTokenRejected = '80210000';
    const BadSecureChannelIdInvalid = '80220000';
    const BadInvalidTimestamp = '80230000';
    const BadNonceInvalid = '80240000';
    const BadSessionIdInvalid = '80250000';
    const BadSessionClosed = '80260000';
    const BadSessionNotActivated = '80270000';
    const BadSubscriptionIdInvalid = '80280000';
    const BadRequestHeaderInvalid = '802A0000';
    const BadTimestampsToReturnInvalid = '802B0000';
    const BadRequestCancelledByClient = '802C0000';
    const BadNoCommunication = '80310000';
    const BadWaitingForInitialData = '80320000';
    const BadNodeIdInvalid = '80330000';
    const BadNodeIdUnknown = '80340000';
    const BadAttributeIdInvalid = '80350000';
    const BadIndexRangeInvalid = '80360000';
    const BadIndexRangeNoData = '80370000';
    const BadDataEncodingInvalid = '80380000';
    const BadDataEncodingUnsupported = '80390000';
    const BadNotReadable = '803A0000';
    const BadNotWritable = '803B0000';
    const BadOutOfRange = '803C0000';
    const BadNotSupported = '803D0000';
    const BadNotFound = '803E0000';
    const BadObjectDeleted = '803F0000';
    const BadNotImplemented = '80400000';
    const BadMonitoringModeInvalid = '80410000';
    const BadMonitoredItemIdInvalid = '80420000';
    const BadMonitoredItemFilterInvalid = '80430000';
    const BadMonitoredItemFilterUnsupported = '80440000';
    const BadFilterNotAllowed = '80450000';
    const BadStructureMissing = '80460000';
    const BadEventFilterInvalid = '80470000';
    const BadContentFilterInvalid = '80480000';
    const BadFilterOperandInvalid = '80490000';
    const BadContinuationPointInvalid = '804A0000';
    const BadNoContinuationPoints = '804B0000';
    const BadReferenceTypeIdInvalid = '804C0000';
    const BadBrowseDirectionInvalid = '804D0000';
    const BadNodeNotInView = '804E0000';
    const BadServerUriInvalid = '804F0000';
    const BadServerNameMissing = '80500000';
    const BadDiscoveryUrlMissing = '80510000';
    const BadSempahoreFileMissing = '80520000';
    const BadRequestTypeInvalid = '80530000';
    const BadSecurityModeRejected = '80540000';
    const BadSecurityPolicyRejected = '80550000';
    const BadTooManySessions = '80560000';
    const BadUserSignatureInvalid = '80570000';
    const BadApplicationSignatureInvalid = '80580000';
    const BadNoValidCertificates = '80590000';
    const BadRequestCancelledByRequest = '805A0000';
    const BadParentNodeIdInvalid = '805B0000';
    const BadReferenceNotAllowed = '805C0000';
    const BadNodeIdRejected = '805D0000';
    const BadNodeIdExists = '805E0000';
    const BadNodeClassInvalid = '805F0000';
    const BadBrowseNameInvalid = '80600000';
    const BadBrowseNameDuplicated = '80610000';
    const BadNodeAttributesInvalid = '80620000';
    const BadTypeDefinitionInvalid = '80630000';
    const BadSourceNodeIdInvalid = '80640000';
    const BadTargetNodeIdInvalid = '80650000';
    const BadDuplicateReferenceNotAllowed = '80660000';
    const BadInvalidSelfReference = '80670000';
    const BadReferenceLocalOnly = '80680000';
    const BadNoDeleteRights = '80690000';
    const BadServerIndexInvalid = '806A0000';
    const BadViewIdUnknown = '806B0000';
    const BadTooManyMatches = '806D0000';
    const BadQueryTooComplex = '806E0000';
    const BadNoMatch = '806F0000';
    const BadMaxAgeInvalid = '80700000';
    const BadHistoryOperationInvalid = '80710000';
    const BadHistoryOperationUnsupported = '80720000';
    const BadWriteNotSupported = '80730000';
    const BadTypeMismatch = '80740000';
    const BadMethodInvalid = '80750000';
    const BadArgumentsMissing = '80760000';
    const BadTooManySubscriptions = '80770000';
    const BadTooManyPublishRequests = '80780000';
    const BadNoSubscription = '80790000';
    const BadSequenceNumberUnknown = '807A0000';
    const BadMessageNotAvailable = '807B0000';
    const BadInsufficientClientProfile = '807C0000';
    const BadTcpServerTooBusy = '807D0000';
    const BadTcpMessageTypeInvalid = '807E0000';
    const BadTcpSecureChannelUnknown = '807F0000';
    const BadTcpMessageTooLarge = '80800000';
    const BadTcpNotEnoughResources = '80810000';
    const BadTcpInternalError = '80820000';
    const BadTcpEndpointUrlInvalid = '80830000';
    const BadRequestInterrupted = '80840000';
    const BadRequestTimeout = '80850000';
    const BadSecureChannelClosed = '80860000';
    const BadSecureChannelTokenUnknown = '80870000';
    const BadSequenceNumberInvalid = '80880000';
    const BadConfigurationError = '80890000';
    const BadNotConnected = '808A0000';
    const BadDeviceFailure = '808B0000';
    const BadSensorFailure = '808C0000';
    const BadOutOfService = '808D0000';
    const BadDeadbandFilterInvalid = '808E0000';
    const BadRefreshInProgress = '80970000';
    const BadConditionAlreadyDisabled = '80980000';
    const BadConditionDisabled = '80990000';
    const BadEventIdUnknown = '809A0000';
    const BadNoData = '809B0000';
    const BadNoBound = '809C0000';
    const BadDataLost = '809D0000';
    const BadDataUnavailable = '809E0000';
    const BadEntryExists = '809F0000';
    const BadNoEntryExists = '80A00000';
    const BadTimestampNotSupported = '80A10000';
    const BadInvalidArgument = '80AB0000';
    const BadConnectionRejected = '80AC0000';
    const BadDisconnect = '80AD0000';
    const BadConnectionClosed = '80AE0000';
    const BadInvalidState = '80AF0000';
    const BadEndOfStream = '80B00000';
    const BadNoDataAvailable = '80B10000';
    const BadWaitingForResponse = '80B20000';
    const BadOperationAbandoned = '80B30000';
    const BadExpectedStreamToBlock = '80B40000';
    const BadWouldBlock = '80B50000';
    const BadSyntaxError = '80B60000';
    const BadMaxConnectionsReached = '80B70000';
    const BadRequestTooLarge = '80B80000';
    const BadResponseTooLarge = '80B90000';
    const BadEventNotAcknowledgeable = '80BB0000';
    const BadInvalidTimestampArgument = '80BD0000';
    const BadProtocolVersionUnsupported = '80BE0000';
    const BadStateNotActive = '80BF0000';
    const BadFilterOperatorInvalid = '80C10000';
    const BadFilterOperatorUnsupported = '80C20000';
    const BadFilterOperandCountMismatch = '80C30000';
    const BadFilterElementInvalid = '80C40000';
    const BadFilterLiteralInvalid = '80C50000';
    const BadIdentityChangeNotSupported = '80C60000';
    const BadNotTypeDefinition = '80C80000';
    const BadViewTimestampInvalid = '80C90000';
    const BadViewParameterMismatch = '80CA0000';
    const BadViewVersionInvalid = '80CB0000';

}