<?php


namespace XiHe\Message;

use XiHe\Publisher\PublishableMessageInterface;
use XiHe\Publisher\PublishAsInterface;

class AbstractMessage implements PublishAsInterface, PublishableMessageInterface
{
    /** @var int */
    protected static $seqNr = 0;
    /**
     * @var array The channels the message should be published on
     */
    protected $channels = array();
    /**@var  string The message publishing name */
    protected $publishAs;
    /** @var  string Makes the message identifiable by receivers */
    protected $messageId;
    /** @var  string */
    protected $event;

    public function getPublishOnChannels(): array
    {
        return $this->channels;
    }

    public function setPublishOnChannels(array $channels)
    {
        $this->channels = $channels;
        return $this;
    }

    public function transformToArray(): array
    {
        return array(
            'class' => \get_class($this),
            'messageId' => $this->getMessageId(),
        );
    }

    /**
     * Returns a identifier for the message
     * @return string
     */
    public function getMessageId(): string
    {
        if (!$this->messageId) {
            list($uSec, $sec) = \explode(" ", microtime());
            $key = $sec . ':' . \str_replace('0.', '', $uSec) . ':' . static::$seqNr++;
            if (static::$seqNr == 1000) {
                static::$seqNr = 0;
            }
            $this->messageId = $this->getPublishAs() . '_' . $key;
        }
        return $this->messageId;
    }

    /**
     * @param string $messageId
     */
    public function setMessageId(string $messageId)
    {
        $this->messageId = $messageId;
        return $this;
    }

    public function getPublishAs(): string
    {
        if (!$this->publishAs) {
            $this->publishAs = \str_replace('\\', '.', \get_class($this));
        }
        return $this->publishAs;
    }

    public function setPublishAs(string $publishAs)
    {
        $this->publishAs = $publishAs;
        return $this;
    }

    public function hydrate(array $data)
    {
        foreach ($data as $key => $val) {
            if (\property_exists(\get_class($this), $key)) {
                $this->$key = $val;
            }
        }
    }
}