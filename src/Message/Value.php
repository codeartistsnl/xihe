<?php

namespace XiHe\Message;

use XiHe\Publisher\TransformerInterface;

class Value extends AbstractMessage implements TransformerInterface
{
    /** @var mixed */
    public $value;

    /** @var string */
    public $quality;

    /** @var \DateTimeInterface */
    public $timeStamp;

    public function __construct($value = null, string $quality = Quality::Good, ?\DateTimeInterface $timeStamp = null)
    {
        $this->value = $value;
        $this->quality = $quality;
        if ($timeStamp) {
            $this->timeStamp = $timeStamp;
        } else {
            $this->timeStamp = new \DateTime();
        }
    }

    public function transformToArray(): array
    {
        return array_merge(
            parent::transformToArray(),
            array(
                'timeStamp' => $this->timeStamp->format('Y-m-d H:i:s.u'),
                'value' => $this->value,
                'quality' => $this->quality,
            )
        );
    }

}