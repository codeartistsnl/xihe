<?php

namespace XiHe\Responder;


use Rx\ObservableInterface;
use Rx\ObserverInterface;
use XiHe\Logging\HasLoggerTrait;
use XiHe\Message\AbstractMessage;

/**
 * The message  dispatcher receives notifications about received messages. When a message was already received
 * then it is ignored. From the received message a AbstractMessage entity is created and forwarded to observers
 * in the application layer
 *
 * Presentation layer
 * @package XiHe\Responder
 */
class MessageDispatcher implements ObservableInterface
{
    use HasLoggerTrait;
    /** @var \stdClass[] */
    protected $receivedMessages = array();

    /** @var \SplObjectStorage */
    protected $observers;

    /**
     * MessageDispatcher constructor.
     */
    public function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }


    public function handleMessage(string $channel, string $event, string $data)
    {
        $message = \json_decode($data);
        if (!isset($message->messageId) || (isset($message->class) && !\class_exists($message->class))) {
            $this->getLogger()->error("invalid message " . $data);
            return;
        }
        if (\strpos($message->messageId, '_')) {
            list($publishAs, $sequence) = \explode('_', $message->messageId);
            if (empty($event)) {
                $event = $publishAs;
            }
        }
        if ($this->isDuplicate($channel, $message->messageId)) {
            $this->getLogger()->debug("duplicate message: " . $message->messageId);
            return;

        }
        $this->registerMessage($channel, $message->messageId);

        /** @var AbstractMessage $entity */
        $entity = new $message->class;
        $entity->hydrate((array)$message);
        $entity->setPublishOnChannels(array($channel));
        $entity->setPublishAs($event);

        /** @var ObserverInterface $observer */
        foreach ($this->observers as $observer) {
            $observer->onNext($entity);
        }

        //  echo 'Channel: ', $event->channel, PHP_EOL;
        //   echo 'Event: ', $event->event, PHP_EOL;
        // echo 'Data: ', $event->data, PHP_EOL;
    }

    protected function isDuplicate(string $channel, $messageId)
    {
        $isDuplicate = false;
        if (isset($this->receivedMessages[$channel])) {
            $isDuplicate = $this->receivedMessages[$channel] == $messageId;
        }
        return $isDuplicate;
    }

    protected function registerMessage(string $channel, $messageId)
    {
        $this->receivedMessages[$channel] = $messageId;
        return $this;
    }

    public function subscribe(ObserverInterface $observer)
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->attach($observer);
        }
        return $this;
    }

    public function unSubscribe(ObserverInterface $observer)
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->detach($observer);
        }
        return $this;
    }
}