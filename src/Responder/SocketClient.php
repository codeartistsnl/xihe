<?php


namespace XiHe\Responder;

use React\EventLoop\LoopInterface;
use React\SocketClient\DnsConnector;
use React\SocketClient\SecureConnector;
use React\SocketClient\TcpConnector;
use React\SocketClient\TimeoutConnector;
use React\Stream\Stream;
use Rx\ObservableInterface;
use Rx\ObserverInterface;
use XiHe\Logging\HasLoggerTrait;
use function EventLoop\setLoop;


class SocketClient implements ObservableInterface
{
    use HasLoggerTrait;
    /** @var \SplObjectStorage */
    protected $observers;

    /** @var  LoopInterface */
    protected $loop;

    /** @var  string */
    protected $host;

    /** @var  int */
    protected $port;

    /** @var  bool */
    protected $secure;


    public function __construct(LoopInterface $loop, string $host, int $port, bool $secure)
    {
        setLoop($loop);
        $this->observers = new \SplObjectStorage();
        $this->loop = $loop;
        $this->host = $host;
        $this->port = $port;
        $this->secure = $secure;
        $this->connect();
    }

    protected function connect()
    {
        $factory = new \React\Dns\Resolver\Factory();
        $resolver = $factory->create('8.8.8.8', $this->loop);

        $connector = new TcpConnector($this->loop);
        $dnsConnector = new DnsConnector($connector, $resolver);
        if ($this->secure) {
            $dnsConnector = new SecureConnector($dnsConnector, $this->loop);
        }

        // time out connection attempt in 15s todo create config
        $timeOutConnector = new TimeoutConnector($dnsConnector, 5, $this->loop);
        $timeOutConnector->create($this->host, $this->port)->then(
            function (Stream $stream) {
                //onFullFilled
                $stream->on('data', function ($data) {
                    /** @var ObserverInterface $observer */
                    foreach ($this->observers as $observer) {
                        $observer->onNext($data);
                    }
                });
                $stream->on('close', function () {
                    sleep(5);
                    $this->connect();
                });
            },
            function ($rejectedInfo) {
                //onRejected
                if ($rejectedInfo instanceof \Exception) {
                    $this->getLogger()->error($rejectedInfo->getMessage());
                }
                sleep(5);
                $this->connect();

            },
            function ($progressInfo) {
                //onProgress
            }
        );
    }

    public function subscribe(ObserverInterface $observer)
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->attach($observer);
        }
    }

    public function unSubscribe(ObserverInterface $observer)
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->detach($observer);
        }
    }

}