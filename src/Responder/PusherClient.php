<?php

namespace XiHe\Responder;

use React\EventLoop\LoopInterface;
use Rx\Disposable\CallbackDisposable;
use Rx\Observable;
use Rx\ObservableInterface;
use Rx\ObserverInterface;
use Rx\SchedulerInterface;
use Rx\Websocket\MessageSubject;
use function EventLoop\setLoop;

/**
 * Class PusherClient
 *
 * The pusher client connects to the pusher webservice api and subscribes to channels. When a message is
 * received on a channel the message is forwarded to callback observers in the presentation layer
 *
 * Session Layer
 * @package XiHe\Responder
 */
class PusherClient
{
    const DEFAULT_URL = 'wss://ws-eu.pusher.com/app/';

    /** @var \Rx\Websocket\Client */
    protected $client;

    /** @var Observable\AnonymousObservable */
    protected $messages;

    /** @var ObservableInterface[] */
    protected $channels = [];

    public function __construct(LoopInterface $loop, string $applicationKey, ?string $url = null)
    {
        setLoop($loop);
        $url = ($url ?: self::DEFAULT_URL) .
            $applicationKey .
            '?client=xihe-client&version=0.0.1&protocol=7';

        //Only create one connection and share the most recent among all subscriber
        $this->client = new \Rx\Websocket\Client($url);
        $this->client->shareReplay(1);
        $this->messages = $this->client
            ->flatMap(function (MessageSubject $messageSubject) {
                return $messageSubject;
            })
            ->map('json_decode');
    }

    public function channel(string $channel): ObservableInterface
    {
        if (isset($this->channels[$channel])) {
            return $this->channels[$channel];
        }

        $channelMessages = $this->messages->filter(function ($event) use ($channel) {
            return isset($event->channel) && $event->channel == $channel;
        });

        $events = Observable::create(function (ObserverInterface $observer, SchedulerInterface $scheduler) use ($channel, $channelMessages) {
            $subscription = $channelMessages
                ->filter(function ($message) {
                    return $message->event !== 'pusher_internal:subscription_succeeded';
                })
                ->subscribe($observer, $scheduler);
            $this->send(['event' => 'pusher:subscribe', 'data' => ['channel' => $channel]]);

            return new CallbackDisposable(function () use ($channel, $subscription) {
                $this->send(['event' => 'pusher:unsubscribe', 'data' => ['channel' => $channel]]);
                $subscription->dispose();
            });
        });

        $this->channels[$channel] = $events->share();
        return $this->channels[$channel];
    }

    public function send(array $message)
    {
        $this->client
            ->take(1)
            ->subscribeCallback(function (MessageSubject $messageSubject) use ($message) {
                $messageSubject->send(\json_encode($message));
            });
    }
}
